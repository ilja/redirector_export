# Redirector Export

[Redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/) is an
add-on that can be used on Firefox and Firefox-based browsers. It allows you
to easily set url patterns from which it will automatically redirect you.

Personally I use it to get away from tracking on the web, and just to get a
better browsing experience in general.

The redirects which are set up, are to redirect me to alternative front-ends,
alternative applications, or otherwise better (from a privacy perspective)
locations.

## Using

To use it, simply install the plug-in and import the `Redirector.json` file
you'll find in this repository.

## Tips & tricks

URL's can be provided with or without protocol, and they can be with or
without http or https. They can sometimes also be provided with or without
subdomain (e.g. http://example.net vs http://www.example.net)

The pattern `^(https?://)?` can be used for the start of the URL if there is no
subdomain.

If there's possible subdomains, you can use `^(https?://)?([^/]*\.)?`.

Explanation of the pattern:
* `^`: Start of a line.
* `(...)?`: The brackets are a capture group. The question marks means "one or
  zero". Basically we're saying "whatever is between the brackets may or may
  not be there".
* `https?://`: This is either "http://" or "https://".
* `[^/]`: Any character, except for a forward slash. We use this because a
  (sub)domain can't contain a slash. It helps distinguish between `myurl.net`
  and `somesite.net/?search=myurl.net`. If we would use `.` (any character)
  instead of `[^/]` (any character except a slash), then the second URL would
  match, which we probably don't want.
* `*`: The preceding pattern can be present any amount. In this case we are
  saying that we expect any character, except for a slash.
* `\.`: This is just a ".". The backslash is an escape character. A `.`
  normally means any character. In this case we say that we want a normal ".".

I also leave some hints from time to time. Sometimes that's alternatives to
the target we are currently using. Sometimes it's an explanation because the
redirect may not be a perfect solution. Basically, I use it as a free form to
notify or help my future self.

It's also possible to use wildcards instead of
[regex](https://en.wikipedia.org/wiki/Regular_expression), but once you start
using regex, well... you can get a lot more precise, so I just keep using
those now.

## License

I don't think this is actually licensable, but if it is, I hereby provide it
with a [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) license.
